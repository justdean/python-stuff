import numpy as np
import matplotlib.pyplot as plt

# Calculate the difference in Z between guassian normal functions
x = y = np.arange(-5.0, 5.0, 0.1)
X,Y = np.meshgrid(x,y)
Z1 = plt.mlab.bivariate_normal(X, Y, 1.0, 2.0, -2.0, -2.0)
Z2 = plt.mlab.bivariate_normal(X, Y, 2.0, 1.0, 1.0, 1.0)
Z = (Z1 - Z2)

CS = plt.contourf(X,Y,Z); plt.colorbar(CS)
plt.savefig('demo1.png')

